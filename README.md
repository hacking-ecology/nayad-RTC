# NAYAD RTC Library

This is a library for PCF2129AT RTC I2C Brick.

PCF2129 is RTC with integrated quartz crystal

### PCF2129 Datasheet

[PCF2129 Datasheet](https://www.nxp.com/docs/en/data-sheet/PCF2129.pdf)

## Usage
### Library Installation

- Download the ZIP file from web site https://gitlab.com/hacking-ecology/nayad-RTC
>	nayad-RTC-main.zip 

- Open the Arduino IDE and add this library through menu **Sketch** -> **Include Library** -> **Add .ZIP Library** and select the zip file : `nayad-RTC-main.zip`
