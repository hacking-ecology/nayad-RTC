#include "NAYAD_RTC.h"

/**
 @brief Constructor
*/
NAYAD_RTC::NAYAD_RTC(uint8_t addr)
{
  _i2caddr = addr;
  Wire.begin();
}

bool NAYAD_RTC::searchDevice(void)
{
  return !(readI2c(_i2caddr) >> 7);
}

void NAYAD_RTC::configure(void)
{
  set24mode();
}

bool NAYAD_RTC::IsDateTimeValid(void)
{
  uint8_t status = readI2c(PCF2129_SECONDS);
  return !(status & _BV(PCF2129_OSF));
}

uint8_t NAYAD_RTC::LastError(void)
{
  return _lastError;
}

DateTime NAYAD_RTC::now(void)
{
  Wire.beginTransmission(_i2caddr);
  Wire.write(PCF2129_SECONDS);
  _lastError = Wire.endTransmission();
  Wire.requestFrom(_i2caddr, (uint8_t)7);
  while (!Wire.available())
    ;
  uint8_t seconds = bcdToDec(Wire.read());
  uint8_t minutes = bcdToDec(Wire.read());
  uint8_t hours = bcdToDec(Wire.read());
  uint8_t days = bcdToDec(Wire.read());
  uint8_t weekdays = bcdToDec(Wire.read());
  uint8_t months = bcdToDec(Wire.read());
  uint16_t years = bcdToDec(Wire.read()) + 2000;

  return DateTime(years, months, weekdays, days, hours, minutes, seconds);
}

/**
 @brief Set to RTC
 @param [in] DateTime DateTime 
*/
void NAYAD_RTC::SetDateTime(const DateTime &dt)
{
  uint8_t status = readI2c(PCF2129_SECONDS);
  status &= ~_BV(PCF2129_OSF); // clear the flag
  writeI2c(PCF2129_SECONDS, status);

  Wire.beginTransmission(_i2caddr);
  Wire.write(PCF2129_SECONDS);
  Wire.write(decToBcd(dt.second()) + 0x80);
  Wire.write(decToBcd(dt.minute()));
  Wire.write(decToBcd(dt.hour()));
  Wire.write(decToBcd(dt.day()));
  Wire.write(decToBcd(dt.week()));
  Wire.write(decToBcd(dt.month()));
  Wire.write(decToBcd(dt.year() - 2000));
  _lastError = Wire.endTransmission();
}

void NAYAD_RTC::set12mode(void)
{
  uint8_t ctrl;
  ctrl = readCtrl();
  ctrl |= PCF2129_CONTROL_12_24;
  writeCtrl(ctrl);
}

/**
 @brief Set to 24 hour mode
*/
void NAYAD_RTC::set24mode(void)
{
  uint8_t ctrl;
  ctrl = readCtrl();
  ctrl &= ~(PCF2129_CONTROL_12_24);
  writeCtrl(ctrl);
}

void NAYAD_RTC::setWeekDays(uint8_t weekDays)
{
  if (weekDays > 6 && weekDays < 0)
  {
    weekDays = 0;
  }
  writeI2c(PCF2129_WEEKDAYS, decToBcd(weekDays));
}
////////////////////////////////////////////////////////////////

/**
 @brief Read I2C Data
 @param [in] address register address 
 @param [out] data read data 
*/
uint8_t NAYAD_RTC::readI2c(uint8_t address)
{
  Wire.beginTransmission(_i2caddr);
  Wire.write(address);
  _lastError = Wire.endTransmission();
  Wire.requestFrom(_i2caddr, (uint8_t)1);
  while (!Wire.available())
    ;
  return Wire.read();
}

/**
 @brief Write I2C Data
 @param [in] address register address 
 @param [in] data write data 
*/
void NAYAD_RTC::writeI2c(uint8_t address, uint8_t data)
{
  Wire.beginTransmission(_i2caddr);
  Wire.write(address);
  Wire.write(data);
  _lastError = Wire.endTransmission();
}

/**
 @brief Read Control Register
 @param [out] data register data 
*/
uint8_t NAYAD_RTC::readCtrl(void)
{
  return readI2c(PCF2129_CONTROL_REGISTERS);
}

/**
 @brief Write Control Register
 @param [in] data register data 
*/
void NAYAD_RTC::writeCtrl(uint8_t data)
{
  writeI2c(PCF2129_CONTROL_REGISTERS, data);
}

////////////////////////////////////////////////////////////////
